from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render,redirect, get_object_or_404
from .models import * 
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django import forms
from django.core.mail import send_mail
from django.conf import settings
from .forms import *
from .utils import *



def home(request):
    products = Product.objects.all()
    if request.method == 'POST':
      searchTerm = request.POST.get("searched")
      results = Product.objects.filter(name__icontains=searchTerm)
      return render(request, 'home.html', {'products': results, 'search_query': searchTerm})

    context = {'products': products}
    return render(request, 'home.html', context)



def cart(request):
  #This code is for rendering data to the cart page
  #data = cartData(request)
  # cartItems = data['cartItems']  
  # data = cartData(request)
  # cartItems = data['cartItems']
  # order = data['order']
  # items = data['items']
  # context = {'items':items, 'order':order, 'cartItems':cartItems}
  return render(request, 'cart.html')


#this function handles user login, ensuring proper validation, authentication, and displays messages accordingly
def login_user(request):
  if request.method == "POST": #It checks if the request method is "POST" (i.e., form submission),
    username = request.POST['username'] #extract username
    password = request.POST['password'] #extract password
    if username and password:  #checking if both fields are not empty
      try:
        user = authenticate(request, username=User.objects.get(email=username), password=password) # Iauthenticates the user
      except:
        user = authenticate(request, username=username, password=password)
      if user is not None: 
        login(request, user) #log in the user
        messages.success(request, ("Login successfull"))
        return redirect('home')
      else:
        messages.error(request, ("There was an error, Please try again"))
        return redirect("login")
    else:
      messages.error(request, "Please enter your username and password")
  else:
    return render(request, 'login.html', {})
  
def logout_user(request):
  logout(request)
  messages.success(request, ("You have been logged out successfully"))
  return redirect('home')

#The register_user function handles user registration by validating the registration form POST data, 
#checking for existing usernames and emails, creating new user objects, and saving them to the database. 
#It also sets appropriate messages for the user and redirects accordingly.
def register_user(request):
  if request.method== "POST":
    username = request.POST['reg_username']
    email = request.POST['email']
    password1 = request.POST['password1']
    password2 = request.POST['password2']
    account_type = request.POST['acctype']
    # Checking the validity of password
    if password1==password2:
      if User.objects.filter(username=username).exists():
        messages.error(request,"Username taken!")
        return redirect ('login')
      elif User.objects.filter(email=email).exists() and email != '':
        messages.warning(request,"An account for the Provided email exists")
        return redirect ('login')
      else:
        if account_type == 'Customer':    
          new_user = User.objects.create_user(username= username, password = password1, email=email)
          new_user.save()
          messages.success(request,"Account created successfully. You can now Login")
          #this block  sends and email to the user's email notifying him for a successfull account creation
          send_mail(
             'AMAZON ACCOUNT REGISTRATION', #subject
             f"Dear {username}, \n \n Your Customer account has successfully been created! You can now login and shop various items with us. \nThank you for choosing us!, \n \n Team Amazon.", #message
             'settings.EMAIL_HOST_USER', #from mail
             [email], # to email  
             fail_silently=False           
          )
          return redirect("login")
        elif account_type == 'Business':
          new_user = User.objects.create_user(username= username, password = password1, email=email, is_staff=True )
          new_user.save()
          return redirect("login")
    else:
      messages.error(request,"The passwords do not match")

  else:
    return render(request, "login.html", {})
  


def about(request):
  return render(request, 'about.html')

# the update_info function handles the process of updating user information. 
#It ensures the user is authenticated, has a related profile, and then allows them to update their information using a form. 
#It also handles form validation and displays appropriate messages to the user.
@login_required
def update_info(request):
  if request.user.is_authenticated:
    try:
      current_user = Profile.objects.get(user__id=request.user.id)
    except Profile.DoesNotExist:
      messages.error(request, "You do not have a related profile.")
      return redirect('home')

    if request.method == 'POST':
      form = UserInfoForm(request.POST, request.FILES, instance=current_user)
      files = request.FILES.getlist('files')
      checked = request.POST.get('delete', None) #if checkbox is ticked it returns delete, otherwise None
      
      if form.is_valid():
        form.save()
        if checked:
          current_user.file1.delete()
          current_user.file2.delete()
          current_user.file3.delete()
          current_user.file1 = ''
          current_user.file2 = ''
          current_user.file3 = ''
        else:
          pass
        for f in files:
          if f.size<5242880:
            current_user.file1 = files[0]
            current_user.file2 = files[1]
            current_user.file3 = files[2]
            current_user.save(update_fields=['file1','file2','file3']) # this only saves changes made to file field
          else:
            messages.error(request, ("You can only upload up to 3 files of maximun size 5MBs!"))
            return render(request, 'update_info.html', {'form':form})
        messages.success(request, ("Your Info Has Been Updated!"))
        return redirect('home')
      else:
        messages.error(request, "Please Fill In To Update")
        return render(request, 'update_info.html', {'form':form})
    else:
      form = UserInfoForm(instance=current_user)
      return render(request, 'update_info.html', {'form':form})
  else:
    messages.error(request, ("You Must Be Logged In!"))
    return redirect('home')
  
@login_required
def update_password(request):
  if request.user.is_authenticated:
    current_user = request.user
    if request.method == 'POST':
      form = ChangePasswordForm(current_user, request.POST)
      if form.is_valid():
        form.save()
        messages.success(request,"Successfully Changed Password. Please login again!")
        #send email to the customer after changing password
        send_mail(
             'AMAZON PASSWORD CHANGE', #subject
             f"Dear {current_user.username}, \n \n Your have successfully changed your password! You can now login using your new password and shop various items with us. \nThank you for choosing us!, \n \n Team Amazon.", #message
             'settings.EMAIL_HOST_USER', #from mail
             [current_user.email], # to email  
             fail_silently=False           
          )
        return redirect('login')
      else:
        for error in list(form.errors.values()):
          messages.error(request, error)
        return redirect('update_password')
    else:
      form = ChangePasswordForm(current_user)
      return render(request, "update_password.html", {'form': form})
  else:
    messages.error(request, "please login to continue")
    return redirect('home')
  
def settings(request):
  return render(request, 'settings.html')

#this block allows business users to add  products to their inventory
@login_required
def add_product(request): # adds product to user's inventory
    try: # check if the user has an associated profile
        current_user = Profile.objects.get(user__id=request.user.id)
    except Profile.DoesNotExist: #if no profile exists for the user, redirect back to home
        messages.error(request, "You are not eligible. Please Contact Us for help")
        return redirect('home')

    if not request.user.is_staff: #  only staff members(or businesses for this case) can add products
        messages.error(request, "You must be a staff member to add a product.")
        return redirect('home')

    if request.method == 'POST': # checks if the page is being submitted
        form = AddProductForm(request.POST, request.FILES) #  creates an instance of our form with data that was submitted from POST
        product_name = request.POST('product_name')
        if form.is_valid():  #checks if all fields in the form are valid
            # Asign the user profile to the product before saving it
            product = form.save(commit=False)
            product.seller = current_user.user
            product.save()
            messages.success(request, "Your Product Has Been Added!")
            send_mail(
             'AMAZON PRODUCT', #subject
             f"Dear {current_user.username}, \n \n Your have successfully added {product_name} to the site!  \nThank you for choosing us!, \n \n Team Amazon.", #message
             'settings.EMAIL_HOST_USER', #from mail
             [current_user.email], # to email  
             fail_silently=False           
            )
            return redirect('home')
        else:
            messages.error(request, "Please Try Again")
            return render(request, 'add_product.html', {'form': form})
    else:
        form = AddProductForm() #  creates an empty form if method is  GET(not POST)
        return render(request, 'add_product.html', {'form': form})

def delete_product(request, product_id): #  deletes a specific product from the database
    product = get_object_or_404(Product, pk=product_id) #  gets the product by its id or returns a  404 error if it doesn't exist
    if request.method == 'POST':
        product.delete() # deletes the product object from the db
    return redirect('my_products') # redirects to my products after deletion
def category(request, foo):
  foo = foo.replace('-', ' ')
  category = Category.objects.get(name=foo)
  products = Product.objects.filter(category=category)
  return render(request, 'category.html', {'products': products, 'category': category})
  

def product(request, pk):
  product = Product.objects.get(id=pk)
  return render(request, 'product.html', {'product':product})

def my_products(request):
    # Retrieve products belonging to the current user
    user_products = Product.objects.filter(seller=request.user)
    # Pass the products to the template context
    return render(request, 'my_products.html', {'user_products': user_products})
    
def view(request,product_id):
    #Logic to fetch product details from the database 
    product = get_object_or_404(Product, pk=product_id)
    # product = {'name': 'Product Name', 'description': 'Product Description', 'price': 1000}
    return render(request, 'view.html', {'product': product})
 

def search(request):
    if request.method == 'GET':
        form = SearchForm(request.GET)
        if form.is_valid():
            search_query = form.cleaned_data['search_query']
          
            results = Product.objects.filter(your_field__icontains=search_query)
            return render(request, 'search.html', {'results': results, 'search_query': search_query})
    return render(request, 'search.html', {'results': None, 'search_query': None})

def business_profile(request, product_id):  
   product = get_object_or_404(Product, pk=product_id)
   seller_profile = Profile.objects.get(user=product.seller)
   context ={
      'product' : product,
      'seller_profile' : seller_profile
   }           
   return render(request, 'business_profile.html', context)
 
 
def checkout(request):
  if request.user.is_authenticated:
    customer = request.user.customer
    order, created = Order.objects.get_or_create(customer=customer, complete=False)
    items = order.orderitem_set.all()
    cartItems = order.get_cart_items
  else:
    items = []
    order = {'gt_cart_total':0, 'get_cart_items':0}
    cartItems = order['get_cart_items']
    
  context = {'items': items, 'order':order}
  return render(request, 'checkout.html', context)


def product(request, pk):
    product = Product.objects.get(id=pk)
    reviews = Review.objects.filter(product=product)
    review_form = ReviewForm()

    if request.method == 'POST':
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            review = review_form.save(commit=False)
            review.user = request.user
            review.product = product
            review.save()
            return redirect('product', pk=pk)

    return render(request, 'product.html', {'product': product, 'reviews': reviews, 'review_form': review_form})