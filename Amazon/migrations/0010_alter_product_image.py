# Generated by Django 5.0.1 on 2024-04-15 23:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Amazon', '0009_alter_product_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(blank=True, default='no_image.png', null=True, upload_to=''),
        ),
    ]
