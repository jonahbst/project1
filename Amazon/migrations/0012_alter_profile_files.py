# Generated by Django 5.0.1 on 2024-04-16 02:38

import Amazon.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Amazon', '0011_profile_files'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='files',
            field=models.FileField(null=True, upload_to='business_showcase/', validators=[Amazon.models.file_size]),
        ),
    ]
