from django.contrib.auth.forms import UserCreationForm, UserChangeForm, SetPasswordForm
from django.contrib.auth.models import User
from django import forms
from django.core.exceptions import ValidationError
from .models import *

class ChangePasswordForm(SetPasswordForm):
	class Meta:
		model = User
		fields = ['new_password1', 'new_password2']
	def __init__(self, *args, **kwargs):
		super(ChangePasswordForm, self).__init__(*args, **kwargs)

class UserInfoForm(forms.ModelForm):
	contact = forms.CharField(max_length=20, required=False, label="Contact", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"Contact", 'style': 'width: 50vh; display: flex;'}))
	address = forms.CharField(max_length=200, required=False, label="Address", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"Address", 'style': 'width: 50vh; display: flex;'}))
	city = forms.CharField(max_length=200, required=False, label="City", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"City", 'style': 'width: 50vh; display: flex;'}))
	street = forms.CharField(max_length=200, required=False, label="Street", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"Street", 'style': 'width: 50vh; display: flex;'}))

	class Meta:
		model = Profile
		fields = ('contact', 'address', 'city', 'street', 'profile_pic')
	def __init__(self, *args, **kwargs):
		super(UserInfoForm, self).__init__(*args, **kwargs)
		self.fields['profile_pic'].required = False

class AddProductForm(forms.ModelForm):
	name = forms.CharField(max_length=20, required=False, label="", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"Product Name"}))
	price = forms.CharField(max_length=200, required=False, label="", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"Price"}))
	description = forms.CharField(max_length=200, required=False, label="", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':"Description"}))
	

	class Meta:
		model = Product
		fields = ('name', 'price', 'description', 'category', 'image')
	def __init__(self, *args, **kwargs):
		super(AddProductForm, self).__init__(*args, **kwargs)
		self.fields['image'].required = False
    
	def __init__(self, *args, **kwargs):
		super(AddProductForm, self).__init__(*args, **kwargs)
		self.fields['category'].widget.attrs.update({'class': 'form-control'})  # Update widget attributes
	def clean_category(self):
		category = self.cleaned_data.get('category')
		if not category:
			raise forms.ValidationError("Please select a category.")
		return category
 
class SearchForm(forms.ModelForm):
	search_query = forms.CharField(label="Search" , max_length =100)


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['comment', 'rate']
